// console.log('test')

/* ARRAY METHODS

    Mutator Methods
        -seeks to modify the contents of an array
        -are function that mutate an array after they are created. These methods manipulate original array performing various tasks such as adding or removing elements.

*/

let fruits = ["Apple","Orange", "Kiwi", "Watermelon"];

/* push()
    - adds an element in the end of an array and returns the array's length
    Syntax: 
        arrayName.push()
*/

console.log("Current Array Fruits:");
console.log(fruits);

// Adding elements
fruits.push("Mango");
console.log(fruits);

let fruitsLength = fruits.push("Melon");
console.log(fruitsLength);
console.log(fruits);

fruits.push("Avocado","Guava");
console.log(fruits);

/* 
    pop()
        - remove the last element in our array and returns the removed element (when applied inside a variable)

        Syntax;
            arrayName.pop()
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from the pop method");
console.log(fruits);

/* 
    unshift 
        - adds one or more element at the beginning of an array
        -return the length of the array (when presented or stored in a variable)

        Syntax:
            unshift(elementA, elementB)
*/

fruits.unshift("Lime","Banana");
console.log("Mutated array from the unshift method");
console.log(fruits);

/* 
    shift()
        -removes an element at the beginning of our array and returns the removed element

    Syntax:
        arrayName.shift()

*/

let removedFruit2 = fruits.shift()
console.log(removedFruit2);
console.log("Mutated array from the shift method");
console.log(fruits);

/* 
    splice()
        - allows to simultaneously remove elements from a specified index number and adds an element

        Syntax:
            arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

let fruitsSplice = fruits.splice(1,2,"Cherry","Lychee");
console.log(fruitsSplice);
console.log("Mutated array from the splice method");
console.log(fruits);

// Using splice without adding elements
let removedSplice = fruits.splice(3,2)
console.log(fruits);
console.log(removedSplice);

/* 
    sort()
        - rearranges the array element in the alphanumeric order

        Syntax:
        arrayName.sort()
*/

fruits.sort();
console.log("Mutated array from the sort method");
console.log(fruits);


let mixedArr = [12, "May", 36, 94, "August", 5,6,3, "September",10,100,1000];
console.log(mixedArr.sort());


/* 
    reverse()
        - reverses the order of the element in an array

        Syntax:
            arrayName.reverse();
*/

fruits.reverse();
console.log("Mutated array from the reverse method");
console.log(fruits);

// For sorting the items in descending order:

fruits.sort().reverse()
console.log(fruits)

	/*MINI ACTIVITY:
	 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console


	 	function registerFruit () {
	 		let doesFruitExist = fruits.includes(fruitName);

	 		if(doesFruitExst) {
	 			alerat(fruitName "is already on our inventory")
	 		} else {
	 			fruits.push(fruitName);
	 			break;
	 			alert("Fruit is now listed in our inventory")
	 		}
	 	}
	 	
	*/

    // function registerFruit (fruitName) {
    //     let doesFruitExist = fruits.includes(fruitName);

    //     if(doesFruitExist) {
    //         alert(fruitName + "is already on our inventory")
    //     } else {
    //         fruits.push(fruitName);
    //         alert("Fruit is now listed in our inventory")
    //     }
    // }

    // registerFruit("Blueberry");
    // console.log(fruits);


let userArray = [];
console.log("Empty array:");
console.log(userArray);

userArray.push("Augustus Vang","Trystan Cunningham","Makena Potts","Amare Tyler");
console.log("Push:");
console.log(userArray);

userArray.unshift("Cynthia Daniel","Aiyana Solomon","Braeden Keller","Pablo Berry");
console.log("Unshift");
console.log(userArray);

userArray.pop();
console.log("Pop");
console.log(userArray);

userArray.shift();
console.log("Shift");
console.log(userArray);

userArray.sort()
console.log("Sort");
console.log(userArray);


/* 
    non-Mutator Methods

    -these are functions that do not modify an array after they are created
    -these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output

*/

let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];
console.log(countries);

/* 
    indexOf 
        - return the index number of the first matching element found in the array. If no match as found, the result will be -1. THe search process will be done from our first element proceeding to the last element

    Syntx:
        arrayName.indexOf(searchVal)
        arrayName.indexOf(searchVal, fromIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf method:" + firstIndex);
firstIndex = countries.indexOf('PH',4);
console.log("Result of indexOf method:" + firstIndex);
firstIndex = countries.indexOf('PH',7);
console.log("Result of indexOf method:" + firstIndex);

console.log(" ");

/* 
        lastIndexOf 
            - returns the index number of the last matching element found in an array. Here the search process will be done from the last element proceeding to the first.

            Syntax:
                arrayName.lastIndexOf(searchValue)
                arrayName.lastIndexOf(searchValue, fromIndex)
*/

let lastIndex = countries.lastIndexOf('PH');
console.log("Result of lastIndexOf method: " + lastIndex)
lastIndex = countries.lastIndexOf('PH',4);
console.log("Result of lastIndexOf method: " + lastIndex)


/* 
        slice()
            - portions/slices elements from our array and returns a new array

            syntax:
                arrayName.slice(startingIndex)
                arrayName.slice(startingIndex,endingIndex)
*/

console.log(countries);

let slicedArrA = countries.slice(2)
console.log("Result from slice method:")
console.log(slicedArrA);
console.log(countries);

let slicedArrB = countries.slice(2,5);
console.log("Result from slice method:")
console.log(slicedArrB);
console.log(countries);

let slicedArrC = countries.slice(-3);
console.log("Result from slice method:")
console.log(slicedArrC);
console.log(countries);

/* 
        toString()
            - returns an array as a string separated by commas
            - is used internally by Js when an object/array needs to be displayed as a text (like in HTML),
            or when an object or array needs to be used as a string.
*/

let stringArray = countries.toString()
console.log("Result from slice method:")
console.log(stringArray);

/* 
        concat()
            combines two or more arrays and returns the combined result

            syntax:
                arrayName.concat(arrayB)
                arrayName.concat(elementB)
*/

let taskArrayA = ["drink HTML","eat javascript"];
let taskArrayB = ["inhale css","breath sass"];
let taskArrayC = ["get git","be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method:")
console.log(tasks);

let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log("Result from concat method:")
console.log(allTasks);

// Combine arrays with elements
let combineTask = taskArrayA.concat("smell express","throw react");
console.log("Result from concat method:")
console.log(combineTask);


/* 
        join()
            - returns an array as a string
            - does not change the original array
            - we can use any separator. The default is comma (,)

            Syntax:
                arrayName.join(separatorString)
*/

let students = ["Elysha","Gab","Ronel","Jean"];
console.log(students.join())
console.log(students.join("\\"))
console.log(students.join(" - "))


/* 
        Iteration Methods
            -are loops designed to perform repetitive tasks in an array, useful for manipulating arraydata
            resulting in complex tasks.
            -normally work with function supplied as an argument
            -aims to evaluate each element in an array

*/

/* 
        foreach
            -similar to for loop that iterates on each array element

            syntax:
                arrayName.forEach(function(individualElement) {
                    statement
                } )
*/

allTasks.forEach(function(task){
    console.log(task)
});

let filteredTasks = [];

allTasks.forEach(function(task){
    console.log(task)
    if(task.length > 10){
        filteredTasks.push(task);
    }

})

console.log(allTasks)
console.log("Result of filteredasks")
console.log(filteredTasks)

/* 
    map()
        - iterates on each element and returns a new array with different value depending on the result of the function's operation
        Syntax:
            arrayName.map(function(individualElement){
                statement
            })
*/

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number){
    console.log(number)
    return number * number;
})

console.log("Original Array")
console.log(numbers)
console.log("Result of map method:")
console.log(numberMap)

/* 
    every()
        - checks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and false if otherwise.

        Syntax:
            arrayName.every(function(individualElement){
                return expression or condition
            })
*/

let allValid = numbers.every((number)=> {
    return (number < 3 );
})

console.log("Result of every method");
console.log(allValid)

/* 
    some()
        - checks if at least one element in the array meet the given condition. Returns a true value if at least one fo the elements meets the given condition and false otherwise
        Syntax:
            arrayName.some(function(ind)}{
                return expression/condition
            })
*/

let someValid = numbers.some(number => {
    return (number < 3 );
})

console.log("Result of some method");
console.log(someValid)

/* 
    filter()
        - returns a new array that contains elements which meets the given condition, it returns an empty array if no elements satisfied the given condition

        Syntax:
            arrayName.filter(function(ind){

            })
*/

let filteredValid = numbers.filter(number => {
    return (number < 3 );
})

console.log("Result of filter method");
console.log(filteredValid)

/* 
    includes()
        - checks if the argument passed can be found in an array
        - ncan be chained after another method. the result of the first method is uded on the second method until all chained methods have been resolved
*/

let products = ["mouse","KEYBOARD","laptop","monitor"];

let filteredProducts = products.filter(product => {

        return product.toLowerCase().includes('a')

})

console.log(filteredProducts)




